# Emblem Changelog

### 0.1.4 (Not released yet)
* Implemented commenter

* Added more syntax for text
* Added missing html tags

### 0.1.3 (24 March, 2015)
* Fixed a crash

### 0.1.1 (10 January, 2015)
* multiline syntax highlighting
* syntax highlighting for helpers inside {{}}
* disabled parser definition
* added default syntax colors

### 0.1
* Basic Syntax highlighting
