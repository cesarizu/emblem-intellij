// This is a generated file. Not intended for manual editing.
package generated.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.emblem.psi.EmblemTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.emblem.psi.*;

public class EmblemIntagImpl extends ASTWrapperPsiElement implements EmblemIntag {

  public EmblemIntagImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EmblemVisitor visitor) {
    visitor.visitIntag(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EmblemVisitor) accept((EmblemVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EmblemBracketed getBracketed() {
    return findChildByClass(EmblemBracketed.class);
  }

  @Override
  @NotNull
  public List<EmblemClassOrId> getClassOrIdList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EmblemClassOrId.class);
  }

  @Override
  @Nullable
  public EmblemIntext getIntext() {
    return findChildByClass(EmblemIntext.class);
  }

  @Override
  @NotNull
  public List<EmblemParseText> getParseTextList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EmblemParseText.class);
  }

  @Override
  @NotNull
  public List<EmblemPropValue> getPropValueList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EmblemPropValue.class);
  }

}
