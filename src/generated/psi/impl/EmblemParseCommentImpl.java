// This is a generated file. Not intended for manual editing.
package generated.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.emblem.psi.EmblemTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.emblem.psi.*;

public class EmblemParseCommentImpl extends ASTWrapperPsiElement implements EmblemParseComment {

  public EmblemParseCommentImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EmblemVisitor visitor) {
    visitor.visitParseComment(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EmblemVisitor) accept((EmblemVisitor)visitor);
    else super.accept(visitor);
  }

}
