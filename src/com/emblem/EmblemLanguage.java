package com.emblem;

import com.intellij.lang.Language;

public class EmblemLanguage extends Language {
    public static final EmblemLanguage INSTANCE = new EmblemLanguage();

    private EmblemLanguage() {
        super("Emblem");
    }
}
