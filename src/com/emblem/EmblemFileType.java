package com.emblem;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;


public class EmblemFileType extends LanguageFileType {
    public static final EmblemFileType INSTANCE = new EmblemFileType();

    private EmblemFileType() {
        super(EmblemLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Emblem file";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Emblem language file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "emblem";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return EmblemIcons.FILE;
    }
}

