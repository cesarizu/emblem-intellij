// This is a generated file. Not intended for manual editing.
package com.emblem.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class EmblemVisitor extends PsiElementVisitor {

  public void visitBracketed(@NotNull EmblemBracketed o) {
    visitPsiElement(o);
  }

  public void visitClassOrId(@NotNull EmblemClassOrId o) {
    visitPsiElement(o);
  }

  public void visitEachHelper(@NotNull EmblemEachHelper o) {
    visitPsiElement(o);
  }

  public void visitInhelper(@NotNull EmblemInhelper o) {
    visitPsiElement(o);
  }

  public void visitIntag(@NotNull EmblemIntag o) {
    visitPsiElement(o);
  }

  public void visitIntagInline(@NotNull EmblemIntagInline o) {
    visitPsiElement(o);
  }

  public void visitIntext(@NotNull EmblemIntext o) {
    visitPsiElement(o);
  }

  public void visitLine(@NotNull EmblemLine o) {
    visitPsiElement(o);
  }

  public void visitParseComment(@NotNull EmblemParseComment o) {
    visitPsiElement(o);
  }

  public void visitParseText(@NotNull EmblemParseText o) {
    visitPsiElement(o);
  }

  public void visitPropValue(@NotNull EmblemPropValue o) {
    visitPsiElement(o);
  }

  public void visitPropertyRecover(@NotNull EmblemPropertyRecover o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
