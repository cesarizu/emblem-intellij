package com.emblem.psi;

import com.emblem.EmblemLanguage;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class EmblemTokenType extends IElementType{

    public EmblemTokenType(@NotNull @NonNls String debugName) {
        super(debugName, EmblemLanguage.INSTANCE);
    }

    @Override
    public String toString() {
        return "EmblemTokenType." + super.toString();
    }
}
