// This is a generated file. Not intended for manual editing.
package com.emblem.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EmblemLine extends PsiElement {

  @Nullable
  EmblemInhelper getInhelper();

  @Nullable
  EmblemIntag getIntag();

  @Nullable
  EmblemIntagInline getIntagInline();

  @Nullable
  EmblemIntext getIntext();

  @Nullable
  EmblemParseComment getParseComment();

}
