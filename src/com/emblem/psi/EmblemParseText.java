// This is a generated file. Not intended for manual editing.
package com.emblem.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EmblemParseText extends PsiElement {

  @Nullable
  EmblemInhelper getInhelper();

}
