// This is a generated file. Not intended for manual editing.
package com.emblem.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EmblemIntag extends PsiElement {

  @Nullable
  EmblemBracketed getBracketed();

  @NotNull
  List<EmblemClassOrId> getClassOrIdList();

  @Nullable
  EmblemIntext getIntext();

  @NotNull
  List<EmblemParseText> getParseTextList();

  @NotNull
  List<EmblemPropValue> getPropValueList();

}
