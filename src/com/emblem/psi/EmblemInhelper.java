// This is a generated file. Not intended for manual editing.
package com.emblem.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EmblemInhelper extends PsiElement {

  @Nullable
  EmblemBracketed getBracketed();

  @Nullable
  EmblemEachHelper getEachHelper();

  @NotNull
  List<EmblemParseText> getParseTextList();

  @NotNull
  List<EmblemPropValue> getPropValueList();

}
