// This is a generated file. Not intended for manual editing.
package com.emblem.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import generated.psi.impl.*;

public interface EmblemTypes {

  IElementType BRACKETED = new EmblemElementType("BRACKETED");
  IElementType CLASS_OR_ID = new EmblemElementType("CLASS_OR_ID");
  IElementType EACH_HELPER = new EmblemElementType("EACH_HELPER");
  IElementType INHELPER = new EmblemElementType("INHELPER");
  IElementType INTAG = new EmblemElementType("INTAG");
  IElementType INTAG_INLINE = new EmblemElementType("INTAG_INLINE");
  IElementType INTEXT = new EmblemElementType("INTEXT");
  IElementType LINE = new EmblemElementType("LINE");
  IElementType PARSE_COMMENT = new EmblemElementType("PARSE_COMMENT");
  IElementType PARSE_TEXT = new EmblemElementType("PARSE_TEXT");
  IElementType PROPERTY_RECOVER = new EmblemElementType("PROPERTY_RECOVER");
  IElementType PROP_VALUE = new EmblemElementType("PROP_VALUE");

  IElementType COMMENT = new EmblemTokenType("COMMENT");
  IElementType CRLF = new EmblemTokenType("CRLF");
  IElementType CSSCLASS = new EmblemTokenType("CSSCLASS");
  IElementType CSSID = new EmblemTokenType("CSSID");
  IElementType EQ = new EmblemTokenType("EQ");
  IElementType HELPER = new EmblemTokenType("HELPER");
  IElementType OPERATOR = new EmblemTokenType("OPERATOR");
  IElementType PROPERTY = new EmblemTokenType("PROPERTY");
  IElementType STRING = new EmblemTokenType("STRING");
  IElementType TAG = new EmblemTokenType("TAG");
  IElementType TEXT = new EmblemTokenType("TEXT");
  IElementType VARIABLE = new EmblemTokenType("VARIABLE");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == BRACKETED) {
        return new EmblemBracketedImpl(node);
      }
      else if (type == CLASS_OR_ID) {
        return new EmblemClassOrIdImpl(node);
      }
      else if (type == EACH_HELPER) {
        return new EmblemEachHelperImpl(node);
      }
      else if (type == INHELPER) {
        return new EmblemInhelperImpl(node);
      }
      else if (type == INTAG) {
        return new EmblemIntagImpl(node);
      }
      else if (type == INTAG_INLINE) {
        return new EmblemIntagInlineImpl(node);
      }
      else if (type == INTEXT) {
        return new EmblemIntextImpl(node);
      }
      else if (type == LINE) {
        return new EmblemLineImpl(node);
      }
      else if (type == PARSE_COMMENT) {
        return new EmblemParseCommentImpl(node);
      }
      else if (type == PARSE_TEXT) {
        return new EmblemParseTextImpl(node);
      }
      else if (type == PROPERTY_RECOVER) {
        return new EmblemPropertyRecoverImpl(node);
      }
      else if (type == PROP_VALUE) {
        return new EmblemPropValueImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
