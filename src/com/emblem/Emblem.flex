package com.emblem;

import com.intellij.openapi.diagnostic.Logger;
import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.TokenType;
import com.emblem.psi.EmblemTypes;

import java.util.ArrayList;
import java.util.List;

%%

%class EmblemLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType

%eof{  return;
%eof}

%{
  private List<Integer> stateList = new ArrayList<>();
  private int previousIndent = 0;
  private int currentIndent = 0;
  private int lineStart = 0;
  private boolean helperFound = false;
  Logger logger = Logger.getInstance("emblem");

  private void popState() {
    if (stateList.size()>0) {
      yybegin(stateList.remove(0));
    }
  }
%}

HTML5TAGS = a|abbr|acronym|address|applet|area|article|aside|audio|b|base|basefont|bdi|bdo|bgsound|big|blink|blockquote|body|br|button|canvas|caption|center|cite|code|col|colgroup|command|data|datalist|dd|del|details|dfn|dir|div|dl|dt|em|embed|fieldset|figcaption|figure|font|footer|form|frame|frameset|h1|h2|h3|h4|h5|h6|head|header|hgroup|hr|html|i|iframe|img|input|ins|isindex|kbd|keygen|label|legend|li|link|listing|main|map|mark|marquee|menu|menuitem|meta|meter|nav|nobr|noframes|noscript|object|ol|optgroup|option|output|p|param|plaintext|pre|progress|q|rp|rt|ruby|s|samp|script|section|select|small|source|spacer|span|strike|strong|style|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|tt|u|ul|var|video|wbr|xmp

CRLF = \n|\r|\r\n
MATCHALL = [^\r\n]+
WHITE_SPACE = [\ \t\f]
COMMENT_START = \/
AFTER_TAG = ({WHITE_SPACE}|{CRLF}|\.|#|\{)
TAG = ({HTML5TAGS}){AFTER_TAG}?
VARIABLE = [^\ \t\f=\'\"\n\r\f\]\}\{\|]+
STRING = ('([^'\\]|\\.)*'|\"([^\"\\]|\\.)*\")
EQ = [=]
TEXTSTART = \||\'|\"|\+
VARIABLESTART = (\#\{)|(\{\{)|(\{\{\{)
HELPER = [a-z]+(-[a-z]+)*
PROPERTY = [^=\ \n\r\f]+\=
WORD = [^=\ \n\r\f\"\'\{|#\[]+
TEXTWORD = [^\ \n\r\f\{#]+
CSSID = [^\.\ \t\f\n\r\f\{:]+
CSSCLASS = [^\#\.\ \t\f\n\r\f\{:]+

%state INTAG
%state COMMENT
%state VARIABLE
%state INHELPER
%state INEACH
%state INWITH
%state INTAG_HELPER
%state MULTILINE_ATTRIBUTES
%state WAITING_VALUE
%state TEXT
%state HELPER
%state TAG
%state CSSORID
%state CHECK_INDENT

%%

<YYINITIAL> {
    "."|"#"                     { yypushback(1); yybegin(CSSORID); }

    {TEXTSTART}                 { currentIndent += 2; yybegin(TEXT); return EmblemTypes.OPERATOR; }

    "="|"=="                    { helperFound = false; yybegin(VARIABLE); return EmblemTypes.OPERATOR; }

    "%"                         { yybegin(TAG); return EmblemTypes.OPERATOR; }

    {TAG}                       { yypushback(1); yybegin(CSSORID); return EmblemTypes.TAG; }

    {TAG}":"                    { yypushback(1); return EmblemTypes.TAG; }

    {COMMENT_START}             { currentIndent += 2; yybegin(COMMENT); return EmblemTypes.OPERATOR; }

    "each"                      { yybegin(INEACH); return EmblemTypes.HELPER; }

    "with"                      { yybegin(INWITH); return EmblemTypes.HELPER; }

    {HELPER}                    { yybegin(INHELPER); return EmblemTypes.HELPER; }

    {HELPER}":"                 { yypushback(1); return EmblemTypes.HELPER; }

    ":"                         { return EmblemTypes.TEXT; }
}

<COMMENT> {
    {MATCHALL}                  { return EmblemTypes.COMMENT; }
    {CRLF}                      { lineStart = zzCurrentPos + 1; previousIndent = currentIndent; currentIndent = 0; stateList.add(0, COMMENT); yybegin(CHECK_INDENT); return EmblemTypes.CRLF; }
}

<CHECK_INDENT> {
    {WHITE_SPACE}+              {
                                  currentIndent = yylength();
                                  if (previousIndent != currentIndent) {
                                    stateList.remove(0);
                                    yybegin(YYINITIAL);
                                    return TokenType.WHITE_SPACE;
                                  }
                                  yybegin(stateList.remove(0));
                                  return TokenType.WHITE_SPACE;
                                }
    .                           { yypushback(1); stateList.remove(0); yybegin(YYINITIAL); }
}

<TEXT> {
    {TEXTWORD}                  { return EmblemTypes.TEXT; }
    {WHITE_SPACE}+              { return TokenType.WHITE_SPACE; }
    {VARIABLESTART}             { helperFound = false; stateList.add(0, TEXT); yybegin(VARIABLE); return EmblemTypes.OPERATOR; }
    #                           { return EmblemTypes.TEXT; }
    {CRLF}                      { lineStart = zzCurrentPos + 1; previousIndent = currentIndent; currentIndent = 0; stateList.add(0, TEXT); yybegin(CHECK_INDENT); return EmblemTypes.CRLF;}
}

<HELPER> {
    {WHITE_SPACE}               { return TokenType.WHITE_SPACE; }
    {HELPER}                    { yybegin(INHELPER); return EmblemTypes.HELPER; }
}

<TAG> ({HELPER}){AFTER_TAG}?    { yypushback(1); yybegin(CSSORID); return EmblemTypes.TAG; }

<CSSORID> {
    "#"{CSSID}                  { return EmblemTypes.CSSID; }

    "."{CSSCLASS}               { return EmblemTypes.CSSCLASS; }

    {WHITE_SPACE}               { yybegin(INTAG); return TokenType.WHITE_SPACE; }

    ":"                         { yybegin(YYINITIAL); return EmblemTypes.OPERATOR; }

    .                           { yypushback(1); yybegin(INTAG); }
}

<INTAG> {

    "= "|"== "                  { helperFound = false; yybegin(VARIABLE); return EmblemTypes.OPERATOR; }

    {EQ}                        { stateList.add(0, INTAG); yybegin(WAITING_VALUE); return EmblemTypes.EQ; }

    \/                          { currentIndent = zzCurrentPos - lineStart; yybegin(COMMENT); return EmblemTypes.OPERATOR; }

    {VARIABLESTART}             { currentIndent = zzCurrentPos - lineStart; yypushback(2); yybegin(TEXT); }

    {PROPERTY}                  { yypushback(1); yybegin(INTAG); return EmblemTypes.PROPERTY; }

    \{                          { helperFound = false; yybegin(INTAG_HELPER); return EmblemTypes.TEXT; }

    \[{CRLF}                    { stateList.add(0, INTAG); yybegin(MULTILINE_ATTRIBUTES); return EmblemTypes.TEXT; }

    {WORD}                      { currentIndent = zzCurrentPos - lineStart; yybegin(TEXT); return EmblemTypes.TEXT; }

    #                           { currentIndent = zzCurrentPos - lineStart; yybegin(TEXT); return EmblemTypes.TEXT; }

    \[                          { currentIndent = zzCurrentPos - lineStart; yybegin(TEXT); return EmblemTypes.TEXT; }

    {WHITE_SPACE}+              { return TokenType.WHITE_SPACE; }

    {CRLF}                      { lineStart = zzCurrentPos + 1; previousIndent = currentIndent; currentIndent = 0; yybegin(YYINITIAL); return EmblemTypes.CRLF; }

    .                           { yypushback(1); yybegin(TEXT); }
}

<INTAG_HELPER> {
    \}                          { yybegin(INTAG); return EmblemTypes.TEXT; }
    {HELPER}                    { if (!helperFound) { helperFound = true; return EmblemTypes.HELPER; } return EmblemTypes.VARIABLE; }
    {PROPERTY}                  { yypushback(1); return EmblemTypes.PROPERTY; }
    {STRING}                    { return EmblemTypes.STRING; }

    {EQ}                        { stateList.add(0, INTAG_HELPER); yybegin(WAITING_VALUE); return EmblemTypes.EQ; }

    {WHITE_SPACE}+              { return TokenType.WHITE_SPACE; }

    {VARIABLE}                  { return EmblemTypes.VARIABLE; }

}

<VARIABLE> {
    \|                          { return EmblemTypes.OPERATOR; }
    {HELPER}" "                 { yypushback(1); if (!helperFound) { helperFound = true; return EmblemTypes.HELPER; } return EmblemTypes.VARIABLE; }
    {WHITE_SPACE}+              { return TokenType.WHITE_SPACE; }
    {STRING}                    { return EmblemTypes.STRING; }
    \}\}                        { popState(); return EmblemTypes.OPERATOR; }
    \}                          { popState(); return EmblemTypes.OPERATOR; }
    {PROPERTY}                  { yypushback(1); return EmblemTypes.PROPERTY; }
    {EQ}                        { stateList.add(0, VARIABLE); yybegin(WAITING_VALUE); return EmblemTypes.EQ; }
    {VARIABLE}                  { return EmblemTypes.VARIABLE; }
}

<INHELPER, INEACH, INWITH> {
    <INEACH> in                 { return EmblemTypes.OPERATOR; }

    <INWITH> as                 { return EmblemTypes.OPERATOR; }

    "["{CRLF}+                  { stateList.add(0, INHELPER); yybegin(MULTILINE_ATTRIBUTES); return EmblemTypes.TEXT; }

    {PROPERTY}                  { yypushback(1); return EmblemTypes.PROPERTY; }

    {TEXTSTART}                 { currentIndent = zzCurrentPos - lineStart; yybegin(TEXT); return EmblemTypes.OPERATOR; }

    {STRING}                    { return EmblemTypes.STRING; }

    {EQ}                        { stateList.add(0, INHELPER); yybegin(WAITING_VALUE); return EmblemTypes.EQ; }

    {WHITE_SPACE}+              { return TokenType.WHITE_SPACE; }

    {VARIABLE}                  { return EmblemTypes.VARIABLE; }
}

<MULTILINE_ATTRIBUTES> {
    {PROPERTY}                  { yypushback(1); return EmblemTypes.PROPERTY; }

    {EQ}                        { stateList.add(0, MULTILINE_ATTRIBUTES); yybegin(WAITING_VALUE); return EmblemTypes.EQ; }

    {STRING}                    { return EmblemTypes.STRING; }

    {WHITE_SPACE}+              { return TokenType.WHITE_SPACE; }

    {CRLF}                      { return EmblemTypes.CRLF; }

    {VARIABLE}                  { return EmblemTypes.VARIABLE; }

    "]"                         { popState(); return EmblemTypes.TEXT; }
}

<WAITING_VALUE> {
    {STRING}                    { popState(); return EmblemTypes.STRING; }

    \{.*\}                      { popState(); return EmblemTypes.VARIABLE; }

    {VARIABLE}                  { popState(); return EmblemTypes.VARIABLE; }

    {WHITE_SPACE}+              { return TokenType.WHITE_SPACE; }

    {CRLF}                      { lineStart = zzCurrentPos + 1; previousIndent = currentIndent; currentIndent = 0; yybegin(YYINITIAL); return EmblemTypes.CRLF; }
}

{CRLF}                          { lineStart = zzCurrentPos + 1; previousIndent = currentIndent; currentIndent = 0; yybegin(YYINITIAL); return EmblemTypes.CRLF; }

{WHITE_SPACE}+                  { currentIndent = yylength(); yybegin(YYINITIAL); return TokenType.WHITE_SPACE; }

.                               { return TokenType.BAD_CHARACTER; }
