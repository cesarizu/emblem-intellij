package com.emblem;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class EmblemIcons {
    public static final Icon FILE = IconLoader.getIcon("/com/emblem/icons/emblem.png");
}
