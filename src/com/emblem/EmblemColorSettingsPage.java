package com.emblem;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Map;

public class EmblemColorSettingsPage implements ColorSettingsPage {
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
            new AttributesDescriptor("Property", EmblemSyntaxHighlighter.PROPERTY),
            new AttributesDescriptor("Comment", EmblemSyntaxHighlighter.COMMENT),
            new AttributesDescriptor("Separator", EmblemSyntaxHighlighter.SEPARATOR),
            new AttributesDescriptor("Operator", EmblemSyntaxHighlighter.OPERATOR),
            new AttributesDescriptor("Tag", EmblemSyntaxHighlighter.TAG),
            new AttributesDescriptor("Variable", EmblemSyntaxHighlighter.VARIABLE),
            new AttributesDescriptor("String", EmblemSyntaxHighlighter.STRING),
            new AttributesDescriptor("Helper", EmblemSyntaxHighlighter.HELPER),
            new AttributesDescriptor("Text", EmblemSyntaxHighlighter.TEXT),
            new AttributesDescriptor("CSS class", EmblemSyntaxHighlighter.CSSCLASS),
            new AttributesDescriptor("CSS id", EmblemSyntaxHighlighter.CSSID),
    };

    @Nullable
    @Override
    public Icon getIcon() {
        return EmblemIcons.FILE;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new EmblemSyntaxHighlighter();
    }

    @NotNull
    @Override
    public String getDemoText() {
        return "h1 Welcome to Emblem\n" +
                "\n" +
                "%blink buy it on Kozmo.com\n\n" +
                "" +
                "footer\n" +
                "  ul\n" +
                "    li Hello\n" +
                "    li Goodbye\n\n" +
                "" +
                ".title Title\n" +
                "\n" +
                "h1.logo Law Blog\n" +
                "\n" +
                "button.btn.btn-large Submit\n\n" +
                "" +
                "#page-content Content\n" +
                "\n" +
                "span#name Bob Lablah\n\n" +
                "" +
                "button.close data-dismiss=\"modal\" x\n" +
                "\n" +
                "/ For Vanilla Handlebars mode only\n" +
                "button class=\"large {{foo}}\" x\n" +
                "\n" +
                "/ For Ember Handlebars\n" +
                "button class=\"large {{unbound foo}}\" x\n" +
                "\n" +
                "/ Shorthand for Ember\n" +
                "button class=foo! x\n\n" +
                "" +
                "| Some content\n" +
                "\n" +
                "p\n" +
                "  | Lorem #{link-to 'something' | ipsum} dolor sit amet, consectetur \n" +
                "    adipisicing elit, sed do eiusmod tempor incididunt ut labore et \n" +
                "    dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud \n" +
                "    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n" +
                "\n" +
                "span.name Your name is {{name}}\n" +
                "          and my name is #{author}\n" +
                "\n" +
                "span\n" +
                "  ' Trailing space\n\n" +
                "" +
                "h1 = name\n" +
                "p\n" +
                "  = intro\n" +
                "  = highlight name\n\n" +
                "" +
                "body\n" +
                "  == outlet\n\n" +
                "" +
                "ul\n" +
                "  each person in people\n" +
                "    li = person\n" +
                "\n" +
                "link-to \"home\" | Link Text\n" +
                "\n" +
                "list nav id=\"nav-bar\" class=\"top\"\n" +
                "  a href=\"url\" = title\n" +
                "\n" +
                "= strong\n" +
                "  = something\n" +
                "\n" +
                "if something\n" +
                "  p Something!\n" +
                "else\n" +
                "  p Something else!\n\n" +
                "" +
                "img src=logoUrl alt=\"Logo\"\n" +
                "\n" +
                "div class=condition:whenTrue:whenFalse\n" +
                "\n" +
                "div class={ condition1:whenTrue:whenFalse condition2:whenTrue:whenFalse }";
    }

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "Emblem";
    }
}