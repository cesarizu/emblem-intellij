// This is a generated file. Not intended for manual editing.
package com.emblem.parser;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static com.emblem.psi.EmblemTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class EmblemParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, null);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    if (t instanceof IFileElementType) {
      r = parse_root_(t, b, 0);
    }
    else {
      r = false;
    }
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return root(b, l + 1);
  }

  /* ********************************************************** */
  // '[' {(VARIABLE|prop_value) CRLF}* ']' CRLF
  public static boolean bracketed(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bracketed")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, BRACKETED, "<bracketed>");
    r = consumeToken(b, "[");
    r = r && bracketed_1(b, l + 1);
    r = r && consumeToken(b, "]");
    r = r && consumeToken(b, CRLF);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // {(VARIABLE|prop_value) CRLF}*
  private static boolean bracketed_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bracketed_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!bracketed_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "bracketed_1", c)) break;
    }
    return true;
  }

  // (VARIABLE|prop_value) CRLF
  private static boolean bracketed_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bracketed_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = bracketed_1_0_0(b, l + 1);
    r = r && consumeToken(b, CRLF);
    exit_section_(b, m, null, r);
    return r;
  }

  // VARIABLE|prop_value
  private static boolean bracketed_1_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bracketed_1_0_0")) return false;
    boolean r;
    r = consumeToken(b, VARIABLE);
    if (!r) r = prop_value(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // (CSSID) | (CSSCLASS)
  public static boolean class_or_id(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "class_or_id")) return false;
    if (!nextTokenIs(b, "<class or id>", CSSCLASS, CSSID)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, CLASS_OR_ID, "<class or id>");
    r = consumeToken(b, CSSID);
    if (!r) r = consumeToken(b, CSSCLASS);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // 'each' VARIABLE OPERATOR VARIABLE CRLF
  public static boolean each_helper(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "each_helper")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EACH_HELPER, "<each helper>");
    r = consumeToken(b, "each");
    r = r && consumeTokens(b, 0, VARIABLE, OPERATOR, VARIABLE, CRLF);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // HELPER|each_helper (((prop_value|STRING)* (OPERATOR parse_text*) !bracketed) | bracketed)
  public static boolean inhelper(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "inhelper")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, INHELPER, "<inhelper>");
    r = consumeToken(b, HELPER);
    if (!r) r = inhelper_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // each_helper (((prop_value|STRING)* (OPERATOR parse_text*) !bracketed) | bracketed)
  private static boolean inhelper_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "inhelper_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = each_helper(b, l + 1);
    r = r && inhelper_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // ((prop_value|STRING)* (OPERATOR parse_text*) !bracketed) | bracketed
  private static boolean inhelper_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "inhelper_1_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = inhelper_1_1_0(b, l + 1);
    if (!r) r = bracketed(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (prop_value|STRING)* (OPERATOR parse_text*) !bracketed
  private static boolean inhelper_1_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "inhelper_1_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = inhelper_1_1_0_0(b, l + 1);
    r = r && inhelper_1_1_0_1(b, l + 1);
    r = r && inhelper_1_1_0_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (prop_value|STRING)*
  private static boolean inhelper_1_1_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "inhelper_1_1_0_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!inhelper_1_1_0_0_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "inhelper_1_1_0_0", c)) break;
    }
    return true;
  }

  // prop_value|STRING
  private static boolean inhelper_1_1_0_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "inhelper_1_1_0_0_0")) return false;
    boolean r;
    r = prop_value(b, l + 1);
    if (!r) r = consumeToken(b, STRING);
    return r;
  }

  // OPERATOR parse_text*
  private static boolean inhelper_1_1_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "inhelper_1_1_0_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, OPERATOR);
    r = r && inhelper_1_1_0_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // parse_text*
  private static boolean inhelper_1_1_0_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "inhelper_1_1_0_1_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!parse_text(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "inhelper_1_1_0_1_1", c)) break;
    }
    return true;
  }

  // !bracketed
  private static boolean inhelper_1_1_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "inhelper_1_1_0_2")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !bracketed(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // OPERATOR? TAG? class_or_id* (prop_value* parse_text* CRLF)|(bracketed intext?)| CRLF
  public static boolean intag(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intag")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, INTAG, "<intag>");
    r = intag_0(b, l + 1);
    if (!r) r = intag_1(b, l + 1);
    if (!r) r = consumeToken(b, CRLF);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // OPERATOR? TAG? class_or_id* (prop_value* parse_text* CRLF)
  private static boolean intag_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intag_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = intag_0_0(b, l + 1);
    r = r && intag_0_1(b, l + 1);
    r = r && intag_0_2(b, l + 1);
    r = r && intag_0_3(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // OPERATOR?
  private static boolean intag_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intag_0_0")) return false;
    consumeToken(b, OPERATOR);
    return true;
  }

  // TAG?
  private static boolean intag_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intag_0_1")) return false;
    consumeToken(b, TAG);
    return true;
  }

  // class_or_id*
  private static boolean intag_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intag_0_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!class_or_id(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "intag_0_2", c)) break;
    }
    return true;
  }

  // prop_value* parse_text* CRLF
  private static boolean intag_0_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intag_0_3")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = intag_0_3_0(b, l + 1);
    r = r && intag_0_3_1(b, l + 1);
    r = r && consumeToken(b, CRLF);
    exit_section_(b, m, null, r);
    return r;
  }

  // prop_value*
  private static boolean intag_0_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intag_0_3_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!prop_value(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "intag_0_3_0", c)) break;
    }
    return true;
  }

  // parse_text*
  private static boolean intag_0_3_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intag_0_3_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!parse_text(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "intag_0_3_1", c)) break;
    }
    return true;
  }

  // bracketed intext?
  private static boolean intag_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intag_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = bracketed(b, l + 1);
    r = r && intag_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // intext?
  private static boolean intag_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intag_1_1")) return false;
    intext(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // TAG class_or_id* ':' line CRLF
  public static boolean intag_inline(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intag_inline")) return false;
    if (!nextTokenIs(b, TAG)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, TAG);
    r = r && intag_inline_1(b, l + 1);
    r = r && consumeToken(b, ":");
    r = r && line(b, l + 1);
    r = r && consumeToken(b, CRLF);
    exit_section_(b, m, INTAG_INLINE, r);
    return r;
  }

  // class_or_id*
  private static boolean intag_inline_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intag_inline_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!class_or_id(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "intag_inline_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // OPERATOR parse_text* CRLF
  public static boolean intext(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intext")) return false;
    if (!nextTokenIs(b, OPERATOR)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, OPERATOR);
    r = r && intext_1(b, l + 1);
    r = r && consumeToken(b, CRLF);
    exit_section_(b, m, INTEXT, r);
    return r;
  }

  // parse_text*
  private static boolean intext_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "intext_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!parse_text(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "intext_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // (intag_inline|intag|(inhelper CRLF)|intext) | parse_comment
  public static boolean line(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "line")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, LINE, "<line>");
    r = line_0(b, l + 1);
    if (!r) r = parse_comment(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // intag_inline|intag|(inhelper CRLF)|intext
  private static boolean line_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "line_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = intag_inline(b, l + 1);
    if (!r) r = intag(b, l + 1);
    if (!r) r = line_0_2(b, l + 1);
    if (!r) r = intext(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // inhelper CRLF
  private static boolean line_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "line_0_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = inhelper(b, l + 1);
    r = r && consumeToken(b, CRLF);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // OPERATOR (COMMENT CRLF)+
  public static boolean parse_comment(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parse_comment")) return false;
    if (!nextTokenIs(b, OPERATOR)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, OPERATOR);
    r = r && parse_comment_1(b, l + 1);
    exit_section_(b, m, PARSE_COMMENT, r);
    return r;
  }

  // (COMMENT CRLF)+
  private static boolean parse_comment_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parse_comment_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = parse_comment_1_0(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!parse_comment_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "parse_comment_1", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // COMMENT CRLF
  private static boolean parse_comment_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parse_comment_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, COMMENT, CRLF);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // TEXT | (OPERATOR VARIABLE OPERATOR) | (OPERATOR inhelper OPERATOR)
  public static boolean parse_text(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parse_text")) return false;
    if (!nextTokenIs(b, "<parse text>", OPERATOR, TEXT)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, PARSE_TEXT, "<parse text>");
    r = consumeToken(b, TEXT);
    if (!r) r = parse_text_1(b, l + 1);
    if (!r) r = parse_text_2(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // OPERATOR VARIABLE OPERATOR
  private static boolean parse_text_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parse_text_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, OPERATOR, VARIABLE, OPERATOR);
    exit_section_(b, m, null, r);
    return r;
  }

  // OPERATOR inhelper OPERATOR
  private static boolean parse_text_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parse_text_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, OPERATOR);
    r = r && inhelper(b, l + 1);
    r = r && consumeToken(b, OPERATOR);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // PROPERTY EQ (VARIABLE|STRING)
  public static boolean prop_value(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prop_value")) return false;
    if (!nextTokenIs(b, PROPERTY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, PROPERTY, EQ);
    r = r && prop_value_2(b, l + 1);
    exit_section_(b, m, PROP_VALUE, r);
    return r;
  }

  // VARIABLE|STRING
  private static boolean prop_value_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prop_value_2")) return false;
    boolean r;
    r = consumeToken(b, VARIABLE);
    if (!r) r = consumeToken(b, STRING);
    return r;
  }

  /* ********************************************************** */
  // !(CRLF|':')
  public static boolean property_recover(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "property_recover")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_, PROPERTY_RECOVER, "<property recover>");
    r = !property_recover_0(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // CRLF|':'
  private static boolean property_recover_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "property_recover_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, CRLF);
    if (!r) r = consumeToken(b, ":");
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // root_item*
  static boolean root(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root")) return false;
    while (true) {
      int c = current_position_(b);
      if (!root_item(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "root", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // !<<eof>> line
  static boolean root_item(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root_item")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = root_item_0(b, l + 1);
    r = r && line(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // !<<eof>>
  private static boolean root_item_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root_item_0")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NOT_);
    r = !eof(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

}
